#include <Arduino.h>
#include "CONTROLS.h"
#include "PINOUT.h"

//#include <STDINT.h>

Controls::Controls()
{

}

Controls::~Controls()
{

}

void Controls::Begin(void)
{
    bitWrite(EncoderADDR,EncoderABit,INPUT);
    bitWrite(EncoderBDDR,EncoderBBit,INPUT);
}

void Controls::Update(void)
{
    static uint8_t LastA=PINK&EncoderAMask;

    if (LastA!=(PINK&EncoderAMask))
    {
        if(LastA^=EncoderAMask)
        {
            if(PINK&EncoderBMask)
            {
                AbsoluteCounter--;
                RelativeCounter--;
            }
            else
            {
                AbsoluteCounter++;
                RelativeCounter++;
            }
        }
        else
        {
            if(PINK&EncoderBMask)
            {
                AbsoluteCounter++;
                RelativeCounter++;
            }
            else
            {
                AbsoluteCounter--;
                RelativeCounter--;
            }
        }
    }
}