#include <Arduino.h>
#include "PINOUT.h"
#include "CONTROLS.h"

Controls ReespiratorControls;

void setup() 
{
  Serial.begin(115200);
  ReespiratorControls.Begin();
}


void loop()
{
  static int16_t Counter;
  delay(1);
  ReespiratorControls.Update();

  if(Counter!=ReespiratorControls.RelativeCounter)
  {
    Counter=ReespiratorControls.RelativeCounter;
    Serial.print(Counter);
    Serial.write("\n\r");
  }
}
