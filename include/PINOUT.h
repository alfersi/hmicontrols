#ifndef PINOUT_H
#define PINOUT_H

#define EncoderAPORT PINK
#define EncoderADDR DDRK
#define EncoderABit 6
#define EncoderBPORT PINK
#define EncoderBDDR DDRK
#define EncoderBBit 7
  
#define EncoderAMask 1<<EncoderABit
#define EncoderBMask 1<<EncoderBBit

#endif