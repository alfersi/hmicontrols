#ifndef CONTROLS_H
#define CONTROLS_H

class Controls
{
    public:
    Controls();
    ~Controls();
    void Update(void);
    void Begin(void);
    int16_t RelativeCounter=0;
    int16_t AbsoluteCounter=0;
    private:
};

#endif